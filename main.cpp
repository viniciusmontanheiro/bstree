#include <iostream>
#include<string>
using namespace std;

#include "BSTree.h"

int main() {
    Person p1,p2,p3,p4,p5,p6,p7;
    BSTree<Person> arvore;

    p1.setCode(10);
    p1.setFistname("Vinícius");
    p1.setLastname("Montanheiro");
    p1.setAge(18);
    p1.setType('m');

    p2.setCode(8);
    p2.setFistname("Fulano");
    p2.setLastname("Silva");
    p2.setAge(45);
    p2.setType('m');

    p3.setCode(4);
    p3.setFistname("Fulana");
    p3.setLastname("Rocha");
    p3.setAge(25);
    p3.setType('f');

    p4.setCode(3);
    p4.setFistname("Cu");
    p4.setLastname("Farias");
    p4.setAge(44);
    p4.setType('f');

    p5.setCode(25);
    p5.setFistname("AAA");
    p5.setLastname("KKK");
    p5.setAge(89);
    p5.setType('m');

    p6.setCode(9);
    p6.setFistname("hahaha");
    p6.setLastname("hohoho");
    p6.setAge(14);
    p6.setType('f');

    p7.setCode(5);
    p7.setFistname("eqwe");
    p7.setLastname("weqwe");
    p7.setAge(16);
    p7.setType('f');

    arvore.add(&arvore.root,p1);
    arvore.add(&arvore.root,p2);
    arvore.add(&arvore.root,p3);
    arvore.add(&arvore.root,p4);
    arvore.add(&arvore.root,p5);
    arvore.add(&arvore.root,p6);
    arvore.add(&arvore.root,p7);


    if(arvore.search(&arvore.root, 4)){
        arvore.showPerson(arvore.getPerson());
    }

    arvore.showAsc(arvore.root);

    return 0;
}