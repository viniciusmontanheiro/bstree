//
// Created by Vinícius Montanheiro on 13/04/15.
//

#ifndef BSTREE_PERSON_H
#define BSTREE_PERSON_H
class Person{
private:
    string fistname;
    string lastname;
    int age;
    int code;
    char type;
public:

    string getFistname() const {
        return this->fistname;
    }

    void setFistname(string fistname) {
        this->fistname = fistname;
    }

    string getLastname() const {
        return this->lastname;
    }

    void setLastname(string lastname) {
        this->lastname = lastname;
    }

    int getAge() const {
        return this->age;
    }

    void setAge(int age) {
        this->age = age;
    }

    int getCode() const {
        return this->code;
    }

    void setCode(int code) {
        this->code = code;
    }

    int getType() const {
        return this->type;
    }

    void setType(char type) {
        this->type = type;
    }

};
#endif //BSTREE_PERSON_H
