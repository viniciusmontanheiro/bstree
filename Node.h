//
// Created by Vinícius Montanheiro on 13/04/15.
//

#ifndef BSTREE_NODE_H
#define BSTREE_NODE_H

template <class T>
class Node{
public:
    T item;
    Node<T> *left;
    Node<T> *right;
    Node<T>(T item);
};

template <class T>
Node<T>::Node(T item){
    this->item = item;
    this->left = 0;
    this->right = 0;
}

#endif //BSTREE_NODE_H
