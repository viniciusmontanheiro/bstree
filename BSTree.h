//
// Árvore de busca binária
// Created by Vinícius Montanheiro on 13/04/15.
//
#include "Node.h"
#include "Comparator.h"
#include <queue>
#include <utility>

#ifndef BSTREE_BSTREE_H
#define BSTREE_BSTREE_H

template<class T>
class BSTree {
public:
    Node<T> *root;

    Validator validator;

    BSTree();

    Person person;

    void add(Node<T> **root, T item);

    bool search(Node<T> **root, int key);

    static Node<T> *moreThenRight(Node<T> **pto);

    static Node<T> *lessThenLeft(Node<T> **pto);

    void remove(Node<T> **root, int key);

    int hight(Node<T> *root);

    void showAsc(Node<T> *root);

    void showDesc(Node<T> *root);

    int amountNodes(Node<T> *root);

    int amountChilds(Node<T> *root);

    void showPerson(Person person);

    Person getPerson() const;

    void setPerson(Person person);

    void levelByLevel(Node<T> *root);

};

template<class T>
BSTree<T>::BSTree() { }

/**
 * @description Adiciona um novo elemento
 * @params Node<T>** , T
 */
template<class T>
void BSTree<T>::add(Node<T> **root, T item) {
    if (!(*root)) {
        *root = new Node<T>(item);
    } else {
        if (this->validator.lessThen((*root)->item, item.getCode())) {
            this->add(&(*root)->right, item);

        } else {
            if (this->validator.moreThen((*root)->item, item.getCode())) {
                this->add(&(*root)->left, item);
            }
        }
    }
}

/**
 * @description Busca um elemento
 * @params Node<T>, int
 */
template<class T>
bool BSTree<T>::search(Node<T> **root, int key) {
    if (!(*root)) {
        return false;
    } else {
        if (this->validator.equal((*root)->item, key)) {
            this->setPerson((*root)->item);
            return true;
        } else {
            if (this->validator.lessThen((*root)->item, key)) {
                return this->search(&(*root)->right, key);
            } else {
                if (this->validator.moreThen((*root)->item, key)) {
                    return this->search(&(*root)->left, key);
                }
            }
        }
    }
}

/**
 * @description Recupera o menor a esquerda
 * @param Node<T>
 */
template<class T>
Node<T> *BSTree<T>::lessThenLeft(Node<T> **pto) {
    if ((*pto)->left) {
        return lessThenLeft(&(*pto)->left);
    } else {
        Node<T> *aux = *pto;
        if ((*pto)->right) {
            *pto = (*pto)->right;
        } else {
            *pto = NULL;
        }
        return aux;
    }
}

/**
 *@description Recupera o maior a direita
 *  * @param Node<T>
 */
template<class T>
Node<T> *BSTree<T>::moreThenRight(Node<T> **pto) {
    if ((*pto)->right) {
        return moreThenRight(&(*pto)->right);
    } else {
        Node<T> *aux = *pto;
        if ((*pto)->left) {
            *pto = (*pto)->left;
        } else {
            *pto = NULL;
        }
        return aux;
    }
}

/**
 * @description Remove um elemento passando a key
 * @params Node<T>, int
 */
template<class T>
void BSTree<T>::remove(Node<T> **root, int key) {

    if (!(*root)) {
        cout << " Árvore vazia..." << endl;
        exit(0);
    }
    //Verifica se o item informado é menor que o item pai da árvore.
    if (this->validator.lessThen((*root)->item, key)) {
        remove(&(*root)->right,
               key); // Se for menor, recursivamente chamamos o remover passando por referência o endereço do item à esquerda da árvore.
    } else {
        //Verifica se o item item informado é maior que o item pai da árvore.
        if (this->validator.moreThen((*root)->item, key)) {
            remove(&(*root)->left,
                   key);// Se for maior, recursivamente chamamos o remover passando por referência o endereço do item à direita da árvore.
        } else {
            Node<T> *pto = *root; //Guardando o item do pai
            if (!(*root)->left && !(*root)->right) {
                (*root) = NULL;
            } else {
                // Possui apenas filhos a direita
                if (!(*root)->left) {
                    (*root) = (*root)->right;
                    pto->right = NULL;
                } else {
                    // Possui apenas filhos a esquerda
                    if (!(*root)->right) {
                        (*root) = (*root)->left;
                        pto->left = NULL;
                    }
                    else {
                        //Buscamos o menor da sub árvore a direita
                        pto = lessThenLeft(&(*root)->right);
                        pto->left = (*root)->left;
                        pto->right = (*root)->right;
                        (*root)->left = (*root)->right = NULL;
                        *root = pto;
                    }
                }
            }

        }
    }
}

/**
 * @description Verifica a altura
 * @param Node<T>
 */
template<class T>
int BSTree<T>::hight(Node<T> *root) {
    if (!root || (!root->left && !root->right)) {
        return 1;
    }
    else {
        return 1 + this->validator.getMoreThenNodes(this->hight(root->left), this->hight(root->right));
    }
}

/**
 * @description Verifica a quantidade de nós
 * @param Node<T>
 */
template<class T>
int BSTree<T>::amountNodes(Node<T> *root) {
    return !root ? 0 : 1 + (this->amountNodes(root->left) + this->amountNodes(root->right));
}

/**
 * @description Verifica a quantidade de filhos
 * @param Node<T>
 */
template<class T>
int BSTree<T>::amountChilds(Node<T> *root) {
    return !root ? 0 : !root->left && !root->right ? 1 : (amountChilds(root->left) + amountChilds(root->right));
}

/**
 * @description Exibe em ordem
 * @param Node<T>
 */
template<class T>
void BSTree<T>::showAsc(Node<T> *root) {
    if (root) {
        this->showAsc(root->left);
        cout << root->item.getCode() << endl << endl;
        this->showAsc(root->right);
    }
}

/**
 * @description Exibe em order decrescente
 * @param Node<T>
 */
template<class T>
void BSTree<T>::showDesc(Node<T> *root) {
    if (root) {
        cout << root->item.getCode() << endl << endl;
        this->showDesc(root->left);
        this->showDesc(root->right);
    }
}

/**
 * @description Exibe os dados da pessoa
 * @params Person
 */
template<class T>
void BSTree<T>::showPerson(Person person) {
    cout << endl;
    cout << "Registro: " << person.getCode() << endl;
    cout << "Nome: " << person.getFistname() << " " << person.getLastname() << endl;
    cout << "Idade: " << person.getAge();
    cout << "\t\tSexo: " << (char) person.getType() << endl << endl;
}

/**
 * @description Recupera uma pessoa
 * @return Person
 */
template<class T>
Person BSTree<T>::getPerson() const {
    return this->person;
}

/**
 * @description Define uma pessoa
 * @params Person
 */
template<class T>
void  BSTree<T>::setPerson(Person person) {
    this->person = person;
}

/**
 * @description Exibe a árvore por level
 * @params Node<T>
 * @author http://articles.leetcode.com/2010/09/printing-binary-tree-in-level-order.html
 */
//TODO *Tratar melhor esse método e adicionar os galhos
template<class T>
void BSTree<T>::levelByLevel(Node<T> *root) {
    if (!root) {
        cerr << "Árvore não existe!";
    }
    queue<Node<T>*> queue;
    int currentLevel = 1;
    int nextLevel = 0;
    queue.push(root);
    while (!queue.empty()) {
        Node<T> *pto = queue.front();
        queue.pop();
        currentLevel--;
        if (pto) {
            cout << pto->item.getCode() << " ";
            queue.push(pto->left);
            queue.push(pto->right);
            nextLevel += 2;
        }
        if (currentLevel == 0) {
            cout << endl;
            currentLevel = nextLevel;
            nextLevel = 0;
        }
    }
}

#endif //BSTREE_BTREE_H
