//
// Created by Vinícius Montanheiro on 13/04/15.
//
#include "Person.h"

#ifndef BSTREE_COMPARATOR_H
#define BSTREE_COMPARATOR_H
template <class T>
class Comparator{
public:
    virtual bool equal(T pA, int key) = 0;
    virtual bool lessThen(T pA, int key) = 0;
    virtual bool moreThen(T pA, int key) = 0;
    virtual int getMoreThenNodes(int itemA,int itemB) = 0;
};
#endif //BSTREE_COMPARATOR_H


#ifndef BSTREE_VALIDATOR_H
#define BSTREE_VALIDATOR_H
class Validator:public Comparator<Person>{
public:
    Validator();
    bool equal(Person pA, int pB);
    bool lessThen(Person pA, int key);
    bool moreThen(Person pA, int key);
    int getMoreThenNodes(int itemA,int itemB);
};
Validator::Validator() { }

bool Validator::equal(Person pA, int key) {
    return pA.getCode() == key;
}

bool Validator::lessThen(Person pA, int key) {
    return pA.getCode() < key;
}

bool Validator::moreThen(Person pA, int key) {
    return pA.getCode() > key;
}

int Validator::getMoreThenNodes(int itemA,int itemB){
    return itemA > itemB ? itemA : itemB;
}
#endif //BSTREE_VALIDATOR_H
